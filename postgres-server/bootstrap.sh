#!/usr/bin/env bash
yinstall="yum install -y"

sudo su
$yinstall epel-release vim net-tools.x86_64 bzip2 htop

# Postgres 9.4
$yinstall http://yum.postgresql.org/9.4/redhat/rhel-7-x86_64/pgdg-centos94-9.4-1.noarch.rpm
$yinstall postgresql94 postgresql94-server postgresql94-contrib
echo pathmunge /usr/pgsql-9.4/bin >> /etc/profile.d/pgsql.sh
/usr/pgsql-9.4/bin/postgresql94-setup initdb
systemctl enable postgresql-9.4
systemctl start postgresql-9.4
