# Configuration of postgresql-9.4 server
The machine is provisioned only to install postgres. Configuration is manual and is included in the following instructions.

*Install postgres on Centos-7*: http://www.unixmen.com/postgresql-9-4-released-install-centos-7/

## Configuration for jira
This guide contains information for setting up a user for jira.

### Creating the jira user on Centos-7

```
sudo su
useradd jiradb
passwd jiradb
```

### Creating the dbuser for jira

```
sudo -i -u postgres
createuser --interactive
psql
	$ ALTER role jiradb with password 'XXX';
	$ ALTER role jiradb with login;
	$ ALTER role postgres with password 'postgres';
createdb -E UNICODE -l C -T template0 jiradb
```

### Change pg_hba.conf

* Change type of authentication
* Add the ip of the jira-server machine

`sudo vim /var/lib/pgsql/9.4/data/pg_hba.conf`

```
# TYPE  DATABASE        USER            ADDRESS                 METHOD

# "local" is for Unix domain socket connections only
local   all             all                                     md5
# IPv4 local connections:
host    all             all             127.0.0.1/32            md5
# IPv6 local connections:
host    all             all             ::1/128                 md5
host	all		all		10.20.30.41/24		md5
# Allow replication connections from localhost, by a user with the
# replication privilege.
#local   replication     postgres                                peer
#host    replication     postgres        127.0.0.1/32            ident
#host    replication     postgres        ::1/128                 ident
```

### Change the postgresql.conf
```
log_timezone = 'Europe/Stockholm'
timezone = 'Europe/Stockholm'
listen_addresses = '*'
```

### Set correct timezone for the database
`/usr/bin/psql -U jiradb -d jiradb`

`SET TIME ZONE 'Europe/Stockholm';`

### Finilize setup for jira
`sudo systemctl restart postgresql-9.4.service`

Go to the jira server and check the database connection
