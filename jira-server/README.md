# Configuration for the jira server
The machine isn't provisioned for setting up jira. The setup is manual.

## Installing java
Guide: http://www.techoism.com/install-java-8-on-centosrhel-65/

```
cd /opt
wget --no-cookies --no-check-certificate --header "Cookie: gpw_e24=http%3A%2F%2Fwww.oracle.com%2F; oraclelicense=accept-securebackup-cookie" "http://download.oracle.com/otn-pub/java/jdk/8u25-b17/jdk-8u25-linux-x64.tar.gz"
tar xzf jdk-8u25-linux-x64.tar.gz
cd /opt/jdk1.8.0_25/
alternatives --install /usr/bin/java java /opt/jdk1.8.0_25/bin/java 2
alternatives --config java $prompt

alternatives --install /usr/bin/jar jar /opt/jdk1.8.0_25/bin/jar 2
alternatives --install /usr/bin/javac javac /opt/jdk1.8.0_25/bin/javac 2
alternatives --install /usr/bin/javaws javaws /opt/jdk1.8.0_25/bin/javaws 2
alternatives --set jar /opt/jdk1.8.0_25/bin/jar
alternatives --set javac /opt/jdk1.8.0_25/bin/javac

java -version

export JAVA_HOME=/opt/jdk1.8.0_25
export JRE_HOME=/opt/jdk1.8.0_25/jre
export PATH=$PATH:/opt/jdk1.8.0_25/bin:/opt/jdk1.8.0_25/jre/bin
```

## Installing Jira
Guide: http://www.techoism.com/how-to-install-jira-on-centosrhel/

```
cd /opt
wget https://www.atlassian.com/software/jira/downloads/binary/atlassian-jira-6.4.7-x64.bin
chmod +x atlassian-jira-6.4.7-x64.bin
./atlassian-jira-6.4.7-x64.bin
```

Configure postgres database in the db machine

`createdb -E UNICODE -l C -T template0 jiradb`

```
cd atlassian/jira/lib/
wget https://jdbc.postgresql.org/download/postgresql-9.4-1203.jdbc4.jar
cd ../../../

$ Configure database
atlassian/jira/bin/config.sh
```
